$(".shareto").click(function () {
    $(".social-share").toggle();
});

$("#menu-toggle").click(function () {
    $(".menu").toggleClass("show");
    $(this).toggleClass("active");
    $("body,html").toggleClass("noscroll");
});
$(".dropmenu").click(function () {
    $(this).toggleClass("show");
    $(this).next(".submenu").toggleClass("show");
    
});
$("#search-toggle").click(function () {
    $(".search-bar").addClass("active");
});

$(".menu-close").click(function () {
    $(".search-bar").removeClass("active");
});






$(document).ready(function () {
    $(".pagination").rPage();
});
var swiper = new Swiper('.index-slider', {
    autoHeight: true,
    autoplay: {
        delay: 8000,
        disableOnInteraction: false,
      },
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});
var swiper = new Swiper('.index-why-slider', {
    autoHeight: false,
    slidesPerView: 1,
    autoplay: {
        delay: 2800,
        disableOnInteraction: false,
      },
    loop: true,
    breakpoints: {
        1024: {
            slidesPerView: 3,
        },
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});


$(function(){
    var len = 8;
    $(".breadcrumb-item").each(function(i){
        if($(this).text().length>len){
            $(this).attr("title",$(this).text());
            var text=$(this).text().substring(0,len-1)+"...";
            $(this).text(text);
        }
    });
});