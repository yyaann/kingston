$(".shareto").click(function () {
    $(".social-share").fadeToggle();
});

$("#menu-toggle").click(function () {
    $(".menu").toggleClass("show");
    $(this).toggleClass("active");
    $("body,html").toggleClass("noscroll");
});
$(".dropmenu").click(function () {
    $(this).toggleClass("show");
    $(this).next(".submenu").toggleClass("show");
    // $(".submenu").toggleClass("show");


});
$("#search-toggle").click(function () {
    $(".search-bar").addClass("active");
});

$(".menu-close").click(function () {
    $(".search-bar").removeClass("active");
});



$(document).ready(function () {
    $(".pagination").rPage();
});



var swiper = new Swiper('.index-slider', {
    autoHeight: true,
    speed: 1200,
    autoplay: {
        delay: 8000,
        disableOnInteraction: false,
    },
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
});
var swiper = new Swiper('.index-why-slider', {
    autoHeight: false,
    slidesPerView: 1,
    speed: 1200,
    autoplay: {
        delay: 2800,
        disableOnInteraction: false,
    },
    loop: true,
    breakpoints: {
        767: {
            slidesPerView: 3,
            autoplay: false,
            loop: false,
        },
    },
    pagination: {
        el: '.index-why-slider .swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.index-why-slider .swiper-button-next',
        prevEl: '.index-why-slider .swiper-button-prev',
    },
});


$(function () { //接程式後，補上限制字數即可
    var len = 16;
    $(".breadcrumb-item.active").each(function (i) {
        if ($(this).text().length > len) {
            $(this).attr("title", $(this).text());
            var text = $(this).text().substring(0, len - 1) + "...";
            $(this).text(text);
        }

    });
});


var page_url = window.location.toString();


$(document).ready(function() {
	var i = -1;
	// 新增年份
	for (i = new Date().getFullYear() - 15; i <= new Date().getFullYear(); i++) {
		addOption(birth_year, i, i - 1909);

	}
	// 新增月份
	for (i = 1; i <= 12; i++) {
	     addOption(birth_month, i, i);
	}
	// 新增天份，先預設31天
	for (i = 1; i <= 31; i++) {
	    addOption(birth_day, i, i);
	}
	
	
});

	 
// 設定每個月份的天數
function setDays(year, month,day) {
	var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var yea = year.options[year.selectedIndex].text;
	var mon = month.options[month.selectedIndex].text;
	var num = monthDays[mon - 1];
	if (mon == 2 && isLeapYear(yea)) {
		num++;
	}
	for (var j = day.options.length - 1; j >=num; j--) {
           day.remove(j);
       }
       for (var i = day.options.length; i <= num; i++) {
           addOption(birth_day,i,i);
       }
}
	 

function isLeapYear(year)
{
	return (year % 4 == 0 || (year % 100 == 0 && year % 400 == 0));// 判斷是否閏年
}
	 

function addOption(selectbox, text, value) {
	var option = document.createElement("option");// 向select尾部新增option
	option.text = text;
    option.value = value;
    selectbox.options.add(option);
};